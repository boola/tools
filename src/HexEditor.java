import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 * @author ola.bengtsson
 *
 */
public class HexEditor {
	String mergedIntelHex = "";
	boolean verbose = false;
	int fwBootLoaderFlashStartAddress = -1;
	int fwBootLoaderFlashEndAddress = -1;
	int fwFlashStartAddress = -1;
	int fwFlashEndAddress = -1;

	/**
	 * Constructs a HexEditor object without any inputs. Use this constructor
	 * if you are lacking a proper customization json buffer.
	 * 
	 * @param verbose If true, verbose option is enabled
	 */
	public HexEditor(boolean verbose) {
		super();

		this.verbose = verbose;
	}

	/**
	 * Constructs a HexEditor object without any inputs. Use this constructor
	 * if you have a proper customization json buffer.
	 * 
	 * @param jsonCustomization JSON formatted customization data. The json object
	 * should contain an object "fw-hw-specifics" that contains:
	 *   "fw-boot-loader-flash-start-address",
	 *   "fw-boot-loader-flash-end-address",
	 *   "fw-flash-start-address" and
	 *   "fw-flash-end-address".
	 * @param verbose If true, verbose option is enabled
	 * 
	 * @throws Exception 
	 */
	public HexEditor(JSONObject jsonCustomization, boolean verbose) throws Exception {
		super();

		this.verbose = verbose;

		try {
			initFromJson(jsonCustomization);
		} catch (Exception e) {
			throw e;
		}
	}

	private void initFromJson(JSONObject jsonCustomization) throws ParseException {
		try {
			// hw-fw specifics
			JSONObject jsonObject             =
					(JSONObject)jsonCustomization.get(Defines.TAG_JSON_CUSTOMIZATION_FW_HW_SPECIFICS);
			// Boot-loader start (and end) address in flash
			fwBootLoaderFlashStartAddress =
					(int)Utility.StringHex2Value((String)jsonObject.get("fw-boot-loader-flash-start-address"));
			fwBootLoaderFlashEndAddress =
					(int)Utility.StringHex2Value((String)jsonObject.get("fw-boot-loader-flash-end-address"));
			// FW start (and end) address in flash
			fwFlashStartAddress =
					(int)Utility.StringHex2Value((String)jsonObject.get("fw-flash-start-address"));
			fwFlashEndAddress =
					(int)Utility.StringHex2Value((String)jsonObject.get("fw-flash-end-address"));
		} finally {
			if (verbose) {
				System.out.println("------------- hw-fw-specifics: ------------- ");
				
				System.out.println("  fwBootLoaderFlashStartAddress = " +
								   Utility.Value2StringHex(fwBootLoaderFlashStartAddress, true));
				System.out.println("  fwBootLoaderFlashEndAddress   = " +
						   Utility.Value2StringHex(fwBootLoaderFlashEndAddress, true));
				System.out.println("  fwFlashStartAddress           = " +
						   Utility.Value2StringHex(fwFlashStartAddress, true));
				System.out.println("  fwFlashEndAddress             = " +
						   Utility.Value2StringHex(fwFlashEndAddress, true));
				
				System.out.println("--------------------------------------------");
			}
		}
	}

	/**
	 * Merges two Intel formatted hex buffers into one. The function takes
	 * all records of sHex0 from hex0FlashStartAddress to hex0FlashEndAddress
	 * (lower and higher addresses are skipped), and all records of sHex1 from
	 * addresses in between hex1FlashStartAddress and hex1FlashEndAddress
	 * (lower and higher addresses are skipped).
	 * 
	 * @param sHex0 Buffer containing Intel Hex Buffer 0.
	 * @param hex0FlashStartAddress Start address of used data of sHex0 buffer
	 * (lower addresses are skipped).
	 * @param hex0FlashEndAddress End address of used data of sHex0 buffer (higher
	 * addresses are skipped).
	 * 
	 * @param sHex1 Buffer containing Intel Hex Buffer 1.
	 * @param hex1FlashStartAddress Start address of used data of sHex1 buffer
	 * (lower addresses are skipped).
	 * @param hex1FlashEndAddress End address of used data of sHex1 buffer (higher
	 * addresses are skipped).
	 */
	public String mergeIntelHexs(String sHex0,
								 int hex0FlashStartAddress,
								 int hex0FlashEndAddress,
								 String sHex1,
								 int hex1FlashStartAddress,
								 int hex1FlashEndAddress) throws IOException, CustomizerException {
		
		mergedIntelHex = "";
		
		if(hex0FlashStartAddress == -1) {
			throw new CustomizerException("Hex0 Flash Start Address is not known");
		}
		if(hex0FlashEndAddress == -1) {
			throw new CustomizerException("Hex0 Flash End Address is not known");
		}
		if(hex1FlashStartAddress == -1) {
			throw new CustomizerException("Hex1 Flash Start Address is not known");
		}
		if(hex1FlashEndAddress == -1) {
			throw new CustomizerException("Hex1 Flash End Address is not known");
		}

		// Add first part
		mergedIntelHex = generateIntelOutput(mergedIntelHex,
											 sHex0,
											 hex0FlashStartAddress,
											 hex0FlashEndAddress,
											 false);
		// Add second part
		mergedIntelHex = generateIntelOutput(mergedIntelHex,
				 							 sHex1,
				 							 hex1FlashStartAddress,
				 							 hex1FlashEndAddress,
				 							 true);
		
		return mergedIntelHex;
	}
	
	/**
	 * Merges an Intel formatted hex buffer (boot-loader) with an Intel
	 * formatted firmware buffer. The start / end addresses for the two
	 * buffer parts are received from the JSON customization buffer.
	 * 
	 * @param sBootLoader Intel formatted hex buffer containing a boot-loader.
	 * @param sFirmware Intel formatted hex buffer containing a firmware.
	 * 
	 * @throws IOException
	 * @throws CustomizerException
	 */
	public String mergeIntelBootLoaderAndFirmware(String sBootLoader,
			 					    			  String sFirmware) throws IOException, CustomizerException {
		mergedIntelHex = "";
		
		if(fwBootLoaderFlashEndAddress == -1) {
			throw new CustomizerException("Boot Loader Flash Start Address is not known");
		}
		if(fwBootLoaderFlashEndAddress == -1) {
			throw new CustomizerException("Boot Loader Flash End Address is not known");
		}
		if(fwFlashStartAddress == -1) {
			throw new CustomizerException("Firmware Flash Start Address is not known");
		}
		if(fwFlashEndAddress == -1) {
			throw new CustomizerException("Firmware Flash End Address is not known");
		}
	
		// Add first part
		mergedIntelHex = generateIntelOutput(mergedIntelHex,
								 			 sBootLoader,
								 			 fwBootLoaderFlashStartAddress,
								 			 fwBootLoaderFlashEndAddress,
								 			 false);
		// Add second part
		mergedIntelHex = generateIntelOutput(mergedIntelHex,
								 			 sFirmware,
								 			 fwFlashStartAddress,
								 			 fwFlashEndAddress,
								 			 true);
		
		return mergedIntelHex;
	}

	private String generateIntelOutput(String sHexToExtend,
			                           String sHex,
			                           int hexFlashStart,
			                           int hexFlashEnd,
			                           boolean writeEof) throws IOException, CustomizerException {
		Scanner scan = new Scanner(sHex);
		long highAddress = 0;
		boolean writeRecord = false;
		
		int[] nofReadRecordTypes = new int[Defines.INTEL_RECORD_TYPE_NOF];
		int[] nofUsedRecordTypes = new int[Defines.INTEL_RECORD_TYPE_NOF];
		for (int i = 0; i < nofReadRecordTypes.length; i++) {
			nofReadRecordTypes[i] = 0;
			nofUsedRecordTypes[i] = 0;
		}
		Record[] potentialRecords = new Record[20];
		int nofPotentialRecords = 0;
		
		scan.reset();
	    while(scan.hasNextLine()){
	    	writeRecord = false;
	        String line = scan.nextLine();
	
	        Record rcd = new Record(line);
	        long absoluteAddress = highAddress | rcd.address;
	        
			switch (rcd.recordType) {
				case Defines.INTEL_RECORD_TYPE_DATA:
					nofReadRecordTypes[rcd.recordType]++;
					
					if ((absoluteAddress >= hexFlashStart) &&
						(absoluteAddress <= (hexFlashEnd - rcd.byteCount))) {
						writeRecord = true;
						
						// If we have potential records (linear address MSB changes), then add them
						// as we have data to add here.
						for (int i = 0; i < nofPotentialRecords; i++) {
							String sRcd = potentialRecords[i].getIntelFormatString();

							sHexToExtend += sRcd + "\r" + "\n";
							nofUsedRecordTypes[potentialRecords[i].recordType]++;
						}
						
						nofPotentialRecords = 0;
					} else {
						// Records should be skipped, as we don't write any data for the addresses.
						nofPotentialRecords = 0;
					}
					break;
				case Defines.INTEL_RECORD_TYPE_END_OF_FILE:
					nofReadRecordTypes[rcd.recordType]++;
					
					writeRecord = writeEof;
					break;
				case Defines.INTEL_RECORD_TYPE_EXTENDED_SEGMENT_ADDRESS:
					nofReadRecordTypes[rcd.recordType]++;
					writeRecord = true;
					break;
				case Defines.INTEL_RECORD_TYPE_START_SEGMENT_ADDRESS:
					nofReadRecordTypes[rcd.recordType]++;
					writeRecord = true;
					break;
				case Defines.INTEL_RECORD_TYPE_EXTENDED_LINEAR_ADDRESS:
					nofReadRecordTypes[rcd.recordType]++;
					highAddress = (rcd.data[0] << 24) | (rcd.data[1] << 16);
					potentialRecords[nofPotentialRecords] = rcd;
					nofPotentialRecords++;
					writeRecord = false;
					break;
				case Defines.INTEL_RECORD_TYPE_START_LINEAR_ADDRESS:
					nofReadRecordTypes[rcd.recordType]++;
					writeRecord = true;
					break;
				default:
					throw new CustomizerException("Record type (" + rcd.recordType + ") not supported");
			}

			if (writeRecord) {
				String sRcd = rcd.getIntelFormatString();

				sHexToExtend += sRcd + "\r" + "\n";
				nofUsedRecordTypes[rcd.recordType]++;
			}
	    }
	    
	    scan.close();
	    
		if (verbose) {
			System.out.println("nofReadRecordTypes = " + Arrays.toString(nofReadRecordTypes));
			System.out.println("nofUsedRecordTypes = " + Arrays.toString(nofUsedRecordTypes));
		}
		
		return sHexToExtend;
	}

}
