import java.util.Arrays;

/**
 * Record class for intel records
 */

/**
 * @author ola.bengtsson
 *
 */
public class Record {
	int byteCount;
	int address;
	int recordType;
	char[] data;
	int checkSum;

	/*
	 * String should be an intel formatted 'hex / text' string
	 * 
	 * Example of intel strings:
	 * 
	 * ":10afd0000000000000000000000000000000000071"
	 * ":020000040000fa"
	 * ":04aff000ffc300009b"
	 * 
	 */
	Record(String sRecord) throws CustomizerException{
		super();
		
		
		if (!sRecord.startsWith(":")) {
			throw new CustomizerException("Record data '" + sRecord + "' does not start with a ':'");
		}
		
		String subS = sRecord.substring(1, 3);
		byteCount = (int)(Utility.StringHex2Value(subS) & 0xFFFF);
		subS = sRecord.substring(3, 7);
		address = (int)(Utility.StringHex2Value(subS) & 0xFFFF);
		subS = sRecord.substring(7, 9);
		recordType = (int)(Utility.StringHex2Value(subS) & 0xFFFF);
		
		data = new char[byteCount];
		int i;
		for (i = 0; i < byteCount; i++) {
			subS = sRecord.substring(9 + i * 2, 11 + i * 2);
			int iTemp = (int)(Utility.StringHex2Value(subS) & 0xFFFF);
			
			data[i] = (char)(iTemp & 0xFF);
		}
		
		subS = sRecord.substring(9 + i * 2, 11 + i * 2);
		checkSum = (int)(Utility.StringHex2Value(subS) & 0xFFFF);
		
		if (CheckSum(this) != checkSum) {
			throw new CustomizerException("Record data '" + sRecord + "' has a wrong check sum!");
		}
	}

	@Override
	public String toString() {
		return "Record [byteCount=" + byteCount + ", address=" + Utility.Value2StringHex(address, true)
				+ ", recordType=" + recordType + ", data="
				+ Arrays.toString(data) + ", checkSum=" + Utility.Value2StringHex((byte)((char)checkSum & 0xFF), true) + "]";
	}


	public int CheckSum(Record record) {
		int checkSum = 0;
		
		checkSum += record.byteCount & 0xFF;
		checkSum += (record.address >> 8) & 0xFF; // MSB Address
		checkSum += record.address & 0xFF;        // LSB Address
		checkSum += record.recordType & 0xFF;
		
		for (int i = 0; i < record.byteCount; i++) {
			checkSum += record.data[i] & 0xFF;
		}
		
		return (0x100 - checkSum) & 0xFF;
	}

	public void ReCalculateCheckSum()
	{
		checkSum = CheckSum(this);
	}
	
	public String getIntelFormatString() {
		String s = new String();
		
		s += ':'; // Start character
		s += Utility.Value2StringHex((byte)((char)(byteCount & 0xFF)), false); // ByteCount
		s += Utility.Value2StringHex((byte)((char)((address >> 8) & 0xFF)), false); // Address - MSB
		s += Utility.Value2StringHex((byte)((char)(address & 0xFF)), false); // Address - LSB
		s += Utility.Value2StringHex((byte)((char)(recordType & 0xFF)), false); // ByteCount

		for (int i = 0; i < byteCount; i++) {
			s += Utility.Value2StringHex((byte)data[i], false); // Data[i]
		}
		
		s += Utility.Value2StringHex((byte)((char)(checkSum & 0xFF)), false); // CheckSum

		return s;
	}
	
}
/*
int readRecord(unsigned char line[], record *rcd)
{
	if (line[0] != ':') {
		rcd->byteCount = 0;
		rcd->recordType = 0xff;
		printf("invalid record\r\n");
		return 0;
	}

	rcd->byteCount = convertHex(&(line[1]));
	rcd->adress = (convertHex(&(line[3])) << 8) + convertHex(&(line[5]));
	rcd->recordType = convertHex(&(line[7]));

	int i;
	for (i = 0; i < rcd->byteCount; i++) {
		rcd->data[i] = convertHex(&(line[9 + i * 2]));
	}
	
	unsigned char checksum = convertHex(&(line[9 + i * 2]));

	//Calculate checksum to control...
	if (checkSum(*rcd) != checksum) {
		printf("Wrong checksum\r\n");
	}

	return i * 2 + 10;
}
*/