import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * @author ola.bengtsson
 *
 */
public class Customizer {

	/**
	 * @param args[]: in=input.hex
	 * 	              cust=customization.json
	 *                out=output.hex
	 *                verbose=CustomizeHelper:Main
	 *                flash-address=0xAABBCCDD
	 *                
	 * @throws CustomizerException On errors
	 */
	public static void main(String[] args) throws CustomizerException {
		int nofArgs = args.length;
		boolean verbose = false;
		String filenameInput = "";
		String filenameOutput = "";
		String filenameCustomization = "";
		long addressFlash = -1;
		String targetType = "";
		JSONParser parser;
		
		// Parse arguments
		for (int i = 0; i < nofArgs; i++) {
			if (args[i].startsWith(Defines.ARG_INPUT)) {
				filenameInput = args[i].substring(Defines.ARG_INPUT.length(), args[i].length());
			} else if (args[i].startsWith(Defines.ARG_CUSTOMIZATION)) {
				filenameCustomization = args[i].substring(Defines.ARG_CUSTOMIZATION.length(), args[i].length());
			} else if (args[i].startsWith(Defines.ARG_OUTPUT)) {
				filenameOutput = args[i].substring(Defines.ARG_OUTPUT.length(), args[i].length());
			} else if (args[i].startsWith(Defines.ARG_VERBOSE)) {
				String s = new String (args[i].substring(Defines.ARG_VERBOSE.length(), args[i].length()));

				if (s.toLowerCase().contains(Defines.ARG_VERBOSE_MAIN)) {
					verbose = true;
				}
			} else if (args[i].startsWith(Defines.ARG_FLASH_ADDRESS)) {
				String s = new String (args[i].substring(Defines.ARG_FLASH_ADDRESS.length(), args[i].length()));
				addressFlash = Utility.StringHex2Value(s);
			} else if (args[i].startsWith(Defines.ARG_TARGET_TYPE)) {
				String s = new String(args[i].substring(Defines.ARG_TARGET_TYPE.length(), args[i].length()));
				
				if (s.equals(Defines.TARGET_TYPE_IDESK) ||
					s.equals(Defines.TARGET_TYPE_ID_MINI) ||
					s.equals(Defines.TARGET_TYPE_SENSE)) {
					targetType = s;
				}
			}
		}
		
		// Setup CustomizeHelper / Overwrite parameters
		try {
			// Json parser
			parser = new JSONParser();
			JSONObject jsonParser = (JSONObject)parser.parse(new FileReader(filenameCustomization));

			CustomizeHelper customizeHelper =
					new CustomizeHelper(jsonParser);
			
			customizeHelper.setArguments(args);
			
			Scanner inScanner = new Scanner(new File(filenameInput));
			customizeHelper.readTargetDataFromInputScanner(inScanner);
			inScanner.close();
			if (verbose) {
				System.out.println(" ------------ TARGET DATA READ (Padding removed) - BEGIN ------------");
				byte[] targetData = customizeHelper.getTargetData();
				targetData = Utility.ShrinkData(targetData, false);
				Utility.printTargetData(targetData.length, targetData, 16, true, true);
				targetData = Utility.ExpandData(targetData, false);
				System.out.println(" ------------ TARGET DATA READ (Padding removed) - END   ------------");
			}
			
			customizeHelper.generateCustomizedTargetData();
			if (verbose) {
				System.out.println(" ------------ TARGET DATA CUSTOMIZED (Padding removed) - BEGIN ------------");
				byte[] targetData = customizeHelper.getTargetData();
				targetData = Utility.ShrinkData(targetData, false);
				Utility.printTargetData(targetData.length, targetData, 16, true, true);
				targetData = Utility.ExpandData(targetData, false);
				System.out.println(" ------------ TARGET DATA CUSTOMIZED (Padding removed) - END   ------------");
			}
			
			inScanner = new Scanner(new File(filenameInput));
			customizeHelper.generateIntelOutput(inScanner);
			inScanner.close();
			
			String outIntelHex = customizeHelper.getOutputIntelHex();
			
			// Write intel to outputFile
			FileWriter fileWriter = new FileWriter(new File(filenameOutput));
			fileWriter.write(outIntelHex);
			fileWriter.flush();
			fileWriter.close();
			

			if (targetType.equals(Defines.TARGET_TYPE_IDESK)) {
				// Make a little merge with customized file and the boot-loader.hex
				System.out.println("-------------- Start merging ---------------------");
				//HexEditor hexEditor = new HexEditor(verbose);
				
				// Json parser
				HexEditor hexEditor = new HexEditor(jsonParser, verbose);

				
				//System.out.println("Reading " + filenameOutput);
				//String sID = readFile(filenameOutput);
				System.out.println("Reading " + "hexfile-boot-loader.hex");
				String sBL = readFile("hexfile-boot-loader.hex");
				
				//String merged = hexEditor.mergeIntelHexs(sBL, 0, 0x27FF,
				//		 outIntelHex, 0x2800, 0x7FFFFFFF);
				String merged = hexEditor.mergeIntelBootLoaderAndFirmware(sBL, outIntelHex);
				
				writeFile("out-merged.hex", merged);
				System.out.println("-------------- Done merging ---------------------");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (verbose) {
				System.out.print(Defines.ARG_INPUT);
				System.out.println("'" + filenameInput + "'");
				System.out.print(Defines.ARG_CUSTOMIZATION);
				System.out.println("'" + filenameCustomization + "'");
				System.out.print(Defines.ARG_OUTPUT);
				System.out.println("'" + filenameOutput + "'");
				System.out.print(Defines.ARG_VERBOSE);
				System.out.println("'" + verbose + "'");
				System.out.print(Defines.ARG_FLASH_ADDRESS);
				System.out.println(Utility.Value2StringHex(addressFlash, false));
				System.out.print(Defines.ARG_TARGET_TYPE);
				System.out.println("'" + targetType + "'");
			}
		}
	}

	private static void writeFile(String sFilename, String sFileContent) throws IOException {
		FileWriter file = new FileWriter(new File(sFilename));
		file.write(sFileContent);
		file.close();
	}

	private static String readFile(String filename) throws IOException {
		String sFile = "";
		int iCh = 0;
		char ch = '0';
		
		FileReader file = new FileReader(new File(filename));
		while ((iCh = file.read()) != -1) {
			ch = (char)(iCh & 0xFF);
			
			sFile += ch;
		}
		file.close();

		return sFile;
	}
}
