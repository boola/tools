
public class CustomizerException extends Exception {
    public CustomizerException(String message) {
        super(message);
    }

    public CustomizerException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
