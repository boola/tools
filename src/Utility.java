
public class Utility {
	
	public static long StringHex2Value(String s) {
		long hexValue = 0;
		int shifter = 0;
		
		for (int i = s.length()-1; i >= 0; i--) {
			char ch = s.charAt(i);
			long chVal = 0;
			
			if ((ch >= '0') && (ch <= '9')) {
				chVal = ch - '0';
			} else if ((ch >= 'a') && (ch <= 'f')) {
				chVal = ch - 'a' + 10;
			} else if ((ch >= 'A') && (ch <= 'F')) {
				chVal = ch - 'A' + 10;
			}
			
			hexValue += (chVal << shifter);
			shifter += 4;
		}
		
		return hexValue;
	}
	
	public static String Value2StringHex(int v, boolean leadingChars) {		
		char[] hexChars = {'0','1','2','3', '4','5','6','7', '8','9','a','b', 'c','d','e','f'};
		String s = new String("");
		int chVal1, chVal0;
		
		if (leadingChars) {
			s += "0x";
		}
		
		for (int i = 3; i >= 0; i--) {
			chVal0 = (v >> (i * 8)) & 0xF;
			chVal1 = (v >> (i * 8 + 4)) & 0xF;
		
			s += hexChars[chVal1];
			s += hexChars[chVal0];
		}
		
		return s;
	}

	public static String Value2StringHex(byte v, boolean leadingChars) {		
		char[] hexChars = {'0','1','2','3', '4','5','6','7', '8','9','a','b', 'c','d','e','f'};
		String s = new String("");
		int chVal1, chVal0;
		
		if (leadingChars) {
			s += "0x";
		}
		
		chVal0 = v & 0xF;
		chVal1 = (v >> 4) & 0xF;
	
		s += hexChars[chVal1];
		s += hexChars[chVal0];
		
		return s;
	}

	public static String Value2StringHex(long v, boolean leadingChars) {		
		char[] hexChars = {'0','1','2','3', '4','5','6','7', '8','9','a','b', 'c','d','e','f'};
		String s = new String("");
		long chVal1, chVal0;
		
		if (leadingChars) {
			s += "0x";
		}
		
		for (int i = 7; i >= 0; i--) {
			chVal0 = (v >> (i * 8)) & 0xF;
			chVal1 = (v >> (i * 8 + 4)) & 0xF;
		
			s += hexChars[(int)chVal1];
			s += hexChars[(int)chVal0];
		}
		
		return s;
	}

	public static void printTargetData(int targetDataSize,
									   byte[] targetData,
									   int columnWidth,
									   boolean leadingRowInfo,
									   boolean clearText) {
		int nofRows = targetDataSize / columnWidth;
		
		if (targetData == null) {
			return;
		}
		
		if (targetDataSize % columnWidth != 0) {
			nofRows++;
		}
		
		for (int row = 0; row < nofRows; row++) {
			if (leadingRowInfo) {
				System.out.print("[" + Utility.Value2StringHex(row * columnWidth, false) + "] ");
			}
			
			for (int col = 0; (col < columnWidth) && ((row * columnWidth + col) < targetDataSize); col++) {
				System.out.print(Utility.Value2StringHex(targetData[row * columnWidth + col], false));
			}
			
			if (clearText) {
				System.out.print(" : ");
				
				for (int col = 0; (col < columnWidth) && ((row * columnWidth + col) < targetDataSize); col++) {
					char ch = (char)targetData[row * columnWidth + col];
					
					if ((ch >= ' ') && (ch <= '~')) {
						System.out.print(ch);
					} else {
						System.out.print('.');
					}
				} 
			}
			
			System.out.println();
		}
	}

	public static byte[] SwapByteArrayAccordingToEndianSettings(
			int dSize, byte[] data,
			String hwInt16Endian, String hwInt32Endian,
			boolean hwInt32FromInt16) throws CustomizerException {

		byte[] bData = new byte[dSize];
		
		bData = data;
		
		// Swap data according to hardware settings
		switch (dSize) {
			case 1:
				break;		
			case 2:
				if (hwInt16Endian.equals("big-endian")) {
					byte tmp = bData[0];
					
					bData[0] = bData[1];
					bData[1] = tmp;
				}
				break;
			case 4:
				if (hwInt32FromInt16) {
					if (hwInt16Endian.equals("big-endian")) {
						byte tmp = bData[0];
						
						bData[0] = bData[1];
						bData[1] = tmp;
						
						tmp = bData[2];
						bData[2] = bData[3];
						bData[3] = tmp;
					}
					
					if (hwInt32Endian.equals("big-endian")) {
						byte tmp0 = bData[0];
						byte tmp1 = bData[1];
						
						bData[0] = bData[2];
						bData[1] = bData[3];
						bData[2] = tmp0;
						bData[3] = tmp1;
					}
				} else {
					if (hwInt32Endian.equals("big-endian")) {
						byte tmp0 = bData[0];
						byte tmp1 = bData[1];
						
						bData[0] = bData[3];
						bData[1] = bData[2];
						bData[2] = tmp1;
						bData[3] = tmp0;							
					}	
				}
				break;
			default:
				throw new CustomizerException("byte arrays of size " + dSize + " is not allowed to swap!");
		}

		return bData;
	}

	public static byte[] GetLittleEndianByteArray(long l, int dSize) throws CustomizerException {
		char ch;
		
		byte[] bData = new byte[dSize];
		
		// Little-endian order
		switch (dSize) {
			case 1:
				ch = (char)(l & 0xFF);
				bData[0] = (byte)ch;
				break;
			case 2:
			case 4:
				for (int i = 0; i < dSize; i++) {
					ch = (char)((l >> (8 * i)) & 0xFF);
					bData[i] = (byte)ch;
				}
				break;
			default:
				throw new CustomizerException("Customizing 'uint' of size " + dSize + " is not allowed.");
		}

		return bData;
	}

	public static long getLongFromByteArrayAccordingToEndianSettings(int dSize,
			byte[] bData, String hwInt16Endian, String hwInt32Endian,
			boolean hwInt32FromInt16) throws CustomizerException {
		
		// Get data in little-endian order
		byte[] data = SwapByteArrayAccordingToEndianSettings(dSize, bData,
															 hwInt16Endian, hwInt32Endian,
															 hwInt32FromInt16);
		
		long l = 0;
		for (int i = 0; i < dSize; i++) {
			long ll = (long)data[i];
			
			l |= ll << (i * 8);
		}
		
		return l;
	}

	public static long zeroBitFields(int bitFieldIdx, int bitFieldSize,
			long value) {
		for (int i = 0; i < bitFieldSize; i++) {
			//long vv = 0x00000001;
			long bZ = 0xFFFFFFFE;
			long bZero = (bZ) << (((long)bitFieldIdx) + ((long)i));
			// We need 1's in the shifted bits
			for (int jj = 0; jj < (bitFieldIdx + i); jj++) {
				bZero |= ((long)1) << jj;
			}
			
			value &= bZero;
		}
		
		return value;
	}

	public static byte[] ByteArrayFromLong(long value, int bSize) {
		byte[] bData = new byte[bSize];
		
		for (int i = 0; i < bSize; i++) {
			char ch = (char)((value >> (i * 8)) & 0xFF);
			bData[i] = (byte)ch;
		}
		
		return bData;
	}

	public static byte[] ShrinkData(byte[] bData, boolean verbose) throws CustomizerException {
		int nofShrinkedBytes = (int)Math.ceil(bData.length / 2);
		byte[] bShrinkedData = new byte[nofShrinkedBytes];
		
		int nofShrinked = 0;
		for (int i = 0; i < bData.length; i++) {
			if ((i % 4 == 0) || (i % 4 == 1)) {
				// Use
				bShrinkedData[nofShrinked] = bData[i];
				nofShrinked++;
			}
		}
		
		if (nofShrinked != nofShrinkedBytes) {
			throw new CustomizerException("NofShrinked (" + nofShrinked +") is not as many as expected (" + nofShrinkedBytes + ")");
		}
		
		if (verbose) {
			System.out.println("Data shrinked from " + bData.length + " to " + nofShrinkedBytes);
		}
		
		return bShrinkedData;
	}

	public static byte[] ExpandData(byte[] bData, boolean verbose) throws CustomizerException {
		int nofExpandedBytes = bData.length * 2;
		byte[] bExpandedData = new byte[nofExpandedBytes];
		
		int nofExpanded = 0;
		for (int i = 0; i < bData.length; i++) {
			if (i % 2 == 0) {
				// Use
				bExpandedData[nofExpanded] = bData[i];
				nofExpanded++;
			} else if (i % 2 == 1) {
				// Use ...
				bExpandedData[nofExpanded] = bData[i];
				nofExpanded++;
				// ... and Expand
				bExpandedData[nofExpanded] = (byte)0;
				nofExpanded++;
				bExpandedData[nofExpanded] = (byte)0;
				nofExpanded++;
			}
		}
		
		if (nofExpanded != nofExpandedBytes) {
			throw new CustomizerException("NofExpanded (" + nofExpanded +") is not as many as expected (" + nofExpandedBytes + ")");
		}
		
		if (verbose) {
			System.out.println("Data expanded from " + bData.length + " to " + nofExpandedBytes);
		}
		
		return bExpandedData;
	}
}
