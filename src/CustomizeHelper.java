import org.json.simple.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * 
 * Class for manipulating (customizing and merging) intel hex files.
 * 
 */
public class CustomizeHelper {	
	
	String filenameInput = "";
	String filenameOutput = "";
	String filenameCustomization = "";
	boolean verbose = false;
	long flashAddress = -1;
	String targetType = "";
	JSONParser parser;
	String hwTargetType = "";
	String hwProcessorName = "";
	byte[] targetData;
	JSONObject jsonAll = null;
	String outIntelHex = "";

	// Flash settings
	long flashNofRows = -1;
	long flashRowSize = -1;
	long flashSize = -1;
	String hwFlashInt16Endian = "little-endian";
	boolean hwFlashInt32FromInt16 = false;
	String hwFlashInt32Endian = "little-endian";
	boolean hwFlashPadding = false;
	boolean hwFlashInt8UnevenAddressAllowed = true;
	boolean hwFlashInt16UnevenAddressAllowed = false;
	boolean hwFlashInt32UnevenAddressAllowed = false;
	boolean hwFlashStringUnevenAddressAllowed = false;
	boolean hwFlashStringStringUnevenAddressAllowed = true;

	/**
	 * Constructor to use when not allowing file system operations.
	 * 
	 * The typical flow becomes:
	 * {@code 
	 * ...
	 * JSONObject jObj = {my-json-object-that contains my customization}
	 * CustomizeHelper customizeHelper = new CustomizeHelper((JSONObject) jsonParser);
	 *		
	 * String[] args = ...
	 * customizeHelper.setArguments(args); // [Optional], use only if overwrite arguments
	 *                                     // are available
	 *		
	 * String myIntelHexFormattedInputThatShouldBeCustomized = ":....\n:...\n:...\n:00000001FF";
	 * Scanner inScanner = new Scanner(myIntelHexFormattedInputThatShouldBeCustomized);
	 * 
	 * customizeHelper.readTargetDataFromInputScanner(inScanner); // Mandatory, reads
	 *                                                            // target data from input data
	 * inScanner.close(); // Close the scanner when done.
     *			
	 * customizeHelper.generateCustomizedTargetData(); // [Mandatory], generates customized target
	 *                                                 // data, i.e. applies customization.
	 *		
	 * inScanner = new Scanner(myIntelHexFormattedInputThatShouldBeCustomized);
	 * customizeHelper.generateIntelOutput(inScanner); // [Mandatory], generates customized Intel
	 *                                                 // hex file formatted string
	 * inScanner.close(); // Close the scanner when done.
	 *	
	 * String outIntelHex = customizeHelper.getOutputIntelHex(); // [Mandatory], gets the customized
	 *                                                           // Intel hex file so it can be used
	 *                                                           // in further processing.
	 * ...
	 * }
	 * 
	 * @param jsonCustomization JSON object of the customization.
	 * 
	 * @throws CustomizerException
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 */
	CustomizeHelper(JSONObject jsonCustomization) throws CustomizerException, FileNotFoundException, IOException, ParseException {
		super();
		
		if (jsonCustomization == null) {
			throw new CustomizerException("jsonCustomization is null");
		}
		
		jsonAll = jsonCustomization;
		
		try {
			initFromJson();
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Constructor to use when allowing file system operations.
	 * 
	 * @param filenameInput Name of the Intel Hex formatted text input file
	 * @param filenameOutput Name of the resulting Intel Hex formatted text output file
	 * @param filenameCustomization Name of the customization file containing the json customizations object
	 * @param verbose If true, verbose option is set 
	 * 
	 * @throws CustomizerException
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 */
	CustomizeHelper(String filenameInput,
			        String filenameOutput,
			        String filenameCustomization,
			        boolean verbose) throws CustomizerException, FileNotFoundException, IOException, ParseException {
		super();
		
		if (filenameInput.length() == 0) {
			throw new CustomizerException("No given input file");
		} else if (filenameOutput.length() == 0) {
			throw new CustomizerException("No given output file");
		} else if (filenameCustomization.length() == 0) {
			throw new CustomizerException("No given customization file");
		}
		
		this.filenameInput = filenameInput;
		this.filenameOutput = filenameOutput;
		this.filenameCustomization = filenameCustomization;
		this.verbose = verbose;
		
		try {
			initFromJson();
		} catch (Exception e) {
			throw e;
		}
	}

	public void setArguments(String[] args) {
		int nofArgs = args.length;
		
		// Parse arguments
		for (int i = 0; i < nofArgs; i++) {
			if (args[i].startsWith(Defines.ARG_VERBOSE)) {
				String s = new String (args[i].substring(Defines.ARG_VERBOSE.length(), args[i].length()));

				if (s.toLowerCase().contains(Defines.ARG_VERBOSE_CUSTOMIZE_HELPER)) {
					verbose = true;
				}
			} else if (args[i].startsWith(Defines.ARG_FLASH_ADDRESS)) {
				String s = new String (args[i].substring(Defines.ARG_FLASH_ADDRESS.length(), args[i].length()));
				flashAddress = Utility.StringHex2Value(s);
			} else if (args[i].startsWith(Defines.ARG_TARGET_TYPE)) {
				String s = new String(args[i].substring(Defines.ARG_TARGET_TYPE.length(), args[i].length()));
				
				if (s.equals(Defines.TARGET_TYPE_IDESK) ||
					s.equals(Defines.TARGET_TYPE_ID_MINI) ||
					s.equals(Defines.TARGET_TYPE_SENSE)) {
					targetType = s;
				}
			}
		}
	}
	
	public void setFlashAddress(long address) {
		System.out.println("SET FLASH ADDRESS " + Utility.Value2StringHex(address, true));
		if (address > 0) {
			if (verbose) {
				System.out.println("Overwriting flash address (" + Utility.Value2StringHex(flashAddress, true) +
								   ") to " + Utility.Value2StringHex(address, true));
			}
			flashAddress = address;
		}
	}
	public long getFlashAddress() {
		return flashAddress;
	}

	/**
	 * Generates customized target data by applying customization.
	 * 
	 * @throws CustomizerException On errors
	 * @throws JSONException On errors
	 */
	public void generateCustomizedTargetData() throws CustomizerException, JSONException {
		// If flash is wasting space - shrink target data so we don't care about the flash padding
		if (hwFlashPadding) {
			try {
				targetData = Utility.ShrinkData(targetData, verbose);
			} catch (Exception e) {
				throw e;
			}
		}
		
		if ((hwTargetType.equals(Defines.TARGET_TYPE_SENSE)) ||
			(hwTargetType.equals(Defines.TARGET_TYPE_ID_MINI)) ||
			(hwTargetType.equals(Defines.TARGET_TYPE_IDESK))) {
			try {
				// Device Config 
				@SuppressWarnings("unchecked")
				List<JSONObject> jsonDeviceConfig = (List<JSONObject>)jsonAll.get(Defines.TAG_JSON_DEVICE_CONFIG);
				parseJsonData(jsonDeviceConfig);
				
				// Sensor Config
				@SuppressWarnings("unchecked")
				List<JSONObject> jsonSensorConfig = (List<JSONObject>)jsonAll.get(Defines.TAG_JSON_SENSOR_CONFIG);
				parseJsonData(jsonSensorConfig);
				
				// WLAN-1 Config
				@SuppressWarnings("unchecked")
				List<JSONObject> jsonWlan1Config = (List<JSONObject>)jsonAll.get(Defines.TAG_JSON_WLAN_1_CONFIG);
				parseJsonData(jsonWlan1Config);

				// WLAN-2 Config
				@SuppressWarnings("unchecked")
				List<JSONObject> jsonWlan2Config = (List<JSONObject>)jsonAll.get(Defines.TAG_JSON_WLAN_2_CONFIG);
				parseJsonData(jsonWlan2Config);
				
				// Key Config
				@SuppressWarnings("unchecked")
				List<JSONObject> jsonKeyConfig = (List<JSONObject>)jsonAll.get(Defines.TAG_JSON_KEY_CONFIG);
				parseJsonData(jsonKeyConfig);
				
				if (verbose) {
					System.out.println(" ------------ TARGET DATA (Padding removed) FROM CUST FILE - BEGIN ------------");
					Utility.printTargetData(targetData.length, targetData, 16, true, true);
					System.out.println(" ------------ TARGET DATA (Padding removed) FROM CUST FILE - END   ------------");
				}
			} catch (Exception e) {
				throw e;
			}
		} else {
			throw new CustomizerException("Target type '" + hwTargetType + "' not supported!");
		}

		// If flash is wasting space - expand target data, thus add flash padding
		if (hwFlashPadding) {
			targetData = Utility.ExpandData(targetData, verbose);
		}
	}
	
	private void parseJsonData(List<JSONObject> jsonList) throws CustomizerException {
		if (jsonList == null) {
			// Config does not have any data, do nothing
			return;
		}
		
		String previousDataType = new String("None");
		for (int i = 0; i < jsonList.size(); i++) {
			JSONObject jObj = (JSONObject)jsonList.get(i);
			
			if (verbose) {
				System.out.println(jObj);
			}
			
			long row = (long)jObj.get(Defines.TAG_JSON_FLASH_ROW_START_INDEX);
			long ind = (long)jObj.get(Defines.TAG_JSON_FLASH_START_INDEX);
			long dataSize = (long)jObj.get(Defines.TAG_JSON_FLASH_SIZE);
			String dataType = (String)jObj.get(Defines.TAG_JSON_DATA_TYPE);
			
			Object data = (Object)jObj.get(Defines.TAG_JSON_DATA);
			String valueName = (String)jObj.get(Defines.TAG_JSON_VALUE_NAME);
			
			byte[] bData;
			
			if (dataType.equals(Defines.TAG_JSON_BIT_FIELDS)) {
				long bitFieldIdx = (long)jObj.get(Defines.TAG_JSON_BIT_FIELD_START_INDEX);
				long bitFieldSize = (long)jObj.get(Defines.TAG_JSON_BIT_FIELD_NOF_BITS);

				bData = new byte[(int)dataSize];
				for (int jj = 0; jj < dataSize; jj++) {
					bData[jj] = targetData[jj + ((int)(row * flashNofRows + ind))];
				}
				
				bData = custData2ByteArray(dataType, dataSize, data,
										   bData,
					                       (int)bitFieldIdx, (int)bitFieldSize);
			} else if (dataType.equals(Defines.TAG_JSON_DATA_TYPE_BYTE_ARRAY)) {
				JSONArray arrayData = (JSONArray)jObj.get(Defines.TAG_JSON_DATA);
				
				bData = jsonArray2ByteArray(dataType, dataSize, arrayData);
			} else {
				// Check for incorrect flash address
				if (dataType.equals(Defines.TAG_JSON_DATA_TYPE_UINT)) {
					if ((!hwFlashInt8UnevenAddressAllowed) && (dataSize == 1) && (ind % 2 == 1)) {
						throw new CustomizerException("Uneven flash address is not allowed for INT-8, add a 1-byte filler prior to " +
													  Defines.TAG_JSON_VALUE_NAME + " = " + valueName);
					} else if ((!hwFlashInt16UnevenAddressAllowed) && (dataSize == 2) && (ind % 2 == 1)) {
						throw new CustomizerException("Uneven flash address is not allowed for INT-16, add a 1-byte filler prior to " +
													  Defines.TAG_JSON_VALUE_NAME + " = " + valueName);
					} else if ((!hwFlashInt32UnevenAddressAllowed) && (dataSize == 4) && (ind % 2 == 1)) {
						throw new CustomizerException("Uneven flash address is not allowed for INT-32, add a 1-byte filler prior to " +
													  Defines.TAG_JSON_VALUE_NAME + " = " + valueName);
					}
				} else if (dataType.equals(Defines.TAG_JSON_DATA_TYPE_STRING)) {
					if ((!hwFlashStringUnevenAddressAllowed) && (ind % 2 == 1)) {
						if ((hwFlashStringStringUnevenAddressAllowed) && (previousDataType.equals(Defines.TAG_JSON_DATA_TYPE_STRING))) {
							// We are safe - it is allowed to use an uneven address if we have a preceeding string.
							if (verbose) {
								System.out.println("Allowing uneven address on consequtive strings.");
							}
						} else {
							throw new CustomizerException("Uneven flash address is not allowed for STRING, add a 1-byte filler prior to " +
														  Defines.TAG_JSON_VALUE_NAME + " = " + valueName);
						}
					}
				}
				
				// Read flash-special if it exists
				JSONObject specialData = null;
				
				try {
					specialData = (JSONObject)jObj.get(Defines.TAG_JSON_FLASH_SPECIAL);
				} catch (Exception e) {
					specialData = null;
				}
				
				if (specialData == null) {
					bData = custData2ByteArray(dataType, dataSize, data);
				} else {
					bData = custData2ByteArray(dataType, dataSize, data, specialData);
				}
			}
			
			if (verbose) {
				System.out.print("DATA BYTE ARRAY = ");
				Utility.printTargetData((int)dataSize, bData, (int)dataSize, false, false);
			}
			
			// Add bData to targetData
			if (bData != null) {
				for (int jj = 0; jj < dataSize; jj++) {
					targetData[jj + ((int)(row * flashRowSize + ind))] = bData[jj];
				}
			}
			
			previousDataType = dataType;
		}
	}

	private byte[] jsonArray2ByteArray(String dataType, long dataSize, JSONArray data) throws CustomizerException {
		
		int dSize = (int)dataSize;
		byte[] bData = new byte[dSize];
		int i = 0;
		
		if (data != null) {
			for (i = 0; i < data.size(); i++) {
				long l = (long)data.get(i);
				char ch = (char)(l & 0xFF);
				
				bData[i] = (byte)ch;
			}
		}

		// Fill the rest with zeros
		for (; i < dSize; i++) {
			bData[i] = 0;
		}

		return bData;
	}
	
	private byte[] custData2ByteArray(String dataType, long dataSize, Object data,
									  byte[] currentByteArray,
									  int bitFieldIdx, int bitFieldSize) throws CustomizerException {
		byte[] bData = null;
		long l = (long)data;
		int dSize = (int)dataSize;
		
		// Get uint from currentByteArray
		long value = Utility.getLongFromByteArrayAccordingToEndianSettings(dSize, currentByteArray,
				   														   hwFlashInt16Endian, hwFlashInt32Endian,
				   														   hwFlashInt32FromInt16);
		// Fibble bit fields
		if ((bitFieldIdx + bitFieldSize) >= ((dSize) * 8)) {
			throw new CustomizerException("Bit field (" + bitFieldIdx + ":" + bitFieldSize +
										  ") configuration does not fit in target of size " + dSize + "bytes");
		}
		if ((int)l > (Math.pow(2, bitFieldSize) - 1)) {
			throw new CustomizerException("Fields of size " + bitFieldSize + " can't store a value of " + (int)l);
		}
		// Zero the bit field
		value = Utility.zeroBitFields(bitFieldIdx, bitFieldSize, value);
		
		// Set the new value
		value |= l << bitFieldIdx;
		
		// Write the result in bData (little-endian order)
		bData = Utility.ByteArrayFromLong(value, dSize);
		
		// Swap bData according to endian settings
		bData = Utility.SwapByteArrayAccordingToEndianSettings(dSize, bData,
				   hwFlashInt16Endian, hwFlashInt32Endian,
				   hwFlashInt32FromInt16);
		
		return bData;
	}
	
	private byte[] custData2ByteArray(String dataType, long dataSize, Object data, JSONObject jObjSpecial) throws CustomizerException {
		
		byte[] bData = null;
		int dSize = (int)dataSize;
		
		if (dataType.equals(Defines.TAG_JSON_DATA_TYPE_UINT)) {
			// uint8, uint16 and uint32
			long l = (long)data;
			
			byte[] bData0 = Utility.GetLittleEndianByteArray(l, dSize);
			
			JSONArray jArr = (JSONArray)jObjSpecial.get(Defines.TAG_JSON_ORDER);
			
			if (jArr.size() != dSize) {
				throw new CustomizerException("Number of indeces are different to flash-size");
			}
			
			int[] indeces = new int[jArr.size()];
			for (int i = 0; i < dSize; i++) {
				long lTemp = (long)jArr.get(i);
				indeces[i] = (int)lTemp;
			}
			
			// Swap data according special flash order
			bData = new byte[dSize];
			
			for (int i = 0; i < dSize; i++) {
				bData[i] = bData0[indeces[i]];
			}
		} else {
			throw new CustomizerException("This function only handles uint wiht 'flash-special' set - all others are dealt with another function!");
		}
		
		return bData;
	}
	
	private byte[] custData2ByteArray(String dataType, long dataSize, Object data) throws CustomizerException {
		
		byte[] bData = null;
		int dSize = (int)dataSize;
		
		if (dataType.equals(Defines.TAG_JSON_DATA_TYPE_UINT)) {
			// uint8, uint16 and uint32
			long l = (long)data;
			
			bData = Utility.GetLittleEndianByteArray(l, dSize);
			
			// Swap data according to hardware settings
			bData = Utility.SwapByteArrayAccordingToEndianSettings(dSize, bData,
	   				   hwFlashInt16Endian, hwFlashInt32Endian,
	   				   hwFlashInt32FromInt16);
		} else if (dataType.equals(Defines.TAG_JSON_DATA_TYPE_STRING)) {
			// Copy string to byte array + '\0'
			String s = (String)data;
			
			if (s.length() >= dSize) {
				throw new CustomizerException("'" + s + "'" + " is longer than allowed (" + (dSize-1) + ")");
			}
			bData = new byte[dSize];

			int i;
			for (i = 0; i < s.length(); i++) {
				char ch = s.charAt(i);
				
				bData[i] = (byte)ch;
			}
			for (; i < dSize; i++) {
				// Fill the remaining size with '\0'.
				bData[i] = '\0';
			}
		} else if (dataType.equals(Defines.TAG_JSON_DATA_TYPE_FILLER)) {
			// Fill dataSize bytes with value according to data
			long l = (long)data;
			byte b = (byte)(l & 0xFF);
			
			bData = new byte[dSize];

			for (int i = 0; i < dataSize; i++) {
				bData[i] = b;
			}
		} else if (dataType.equals(Defines.TAG_JSON_DATA_TYPE_BYTE_ARRAY)) {
			throw new CustomizerException("byte-array customization should be handled by jsonArray2ByteArray()");
		} else if (dataType.equals(Defines.TAG_JSON_DATA_TYPE_BIT_FIELD)) {
			throw new CustomizerException("bit-field customization should be handled by another custData2ByteArray()");
		} else if (dataType.equals(Defines.TAG_JSON_DATA_TYPE_IP_ADDRESS)) {
			// "192.168.1.10"			
			String s = (String)data;
			int fromIdx = 0;
			int dotIdx = -1;
			int nofDots = 0;
			int[] dotIdxs = new int[4];
			boolean doneWithDotCount = false;
			
			// Find all '.'
			while (!doneWithDotCount) {
				dotIdx = s.indexOf('.', fromIdx);

				// We must, at least, have one character after the last dot.
				if ((dotIdx > fromIdx) && (dotIdx < (s.length() - 1))) {
					dotIdxs[nofDots] = dotIdx;
					fromIdx = dotIdx + 1;
					nofDots++;
				} else {
					dotIdxs[nofDots] = s.length();
					doneWithDotCount = true;
				}
				
				if (nofDots > 3) {
					throw new CustomizerException("ip address '" + s + "' contains more dots than 3");
				}
			}
			
			// The IP address must contain exactly 3 dots
			if (nofDots != 3) {
				throw new CustomizerException("ip address '" + s + "' does not contain 3 dots");
			}

			bData = new byte[dSize];

			int[] ipVals = new int[4];
			int dotIdx0 = 0, dotIdx1;
			for (int kk = 0; kk < 4; kk++) {
				dotIdx1 = dotIdxs[kk];
				String subS = s.substring(dotIdx0, dotIdx1);
				
				ipVals[kk] = Integer.parseInt(subS);
				char ch = (char)(ipVals[kk] & 0xFF);
				// 1.2.3.4 = 0x01.0x02.0x03.0x04 => INT(0x01020304)
				bData[3 - kk] = (byte)ch;
				
				dotIdx0 = dotIdx1 + 1;
			}
			
			bData = Utility.SwapByteArrayAccordingToEndianSettings(dSize, bData,
												   				   hwFlashInt16Endian, hwFlashInt32Endian,
												   				   hwFlashInt32FromInt16);
			
			if (verbose) {
				System.out.println("IpAddress '" + s + "' parsed as: " +
								   ipVals[0] + ":" + ipVals[1] + ":" +
								   ipVals[2] + ":" + ipVals[3]);
			}
		}
		
		return bData;
	}

	private void initFromJson() throws CustomizerException, FileNotFoundException, IOException, ParseException {
		if (jsonAll == null) {
			if (filenameCustomization.length() == 0) {
				throw new CustomizerException("Can't init from json, as no custmization data exists.");
			}
			parser = new JSONParser();
		}
		int targetDataSize = -1;
		
		try {
			if (jsonAll == null) {
				// Json parser
				Object jsonParser = parser.parse(new FileReader(filenameCustomization));
				// Json parser as Json object
				jsonAll = (JSONObject) jsonParser;
			}

			// hw-fw specifics
			JSONObject jsonObject             =
					(JSONObject)jsonAll.get(Defines.TAG_JSON_CUSTOMIZATION_FW_HW_SPECIFICS);
			
			hwTargetType                      =
					(String)jsonObject.get(Defines.TAG_JSON_HW_TARGET_TYPE);
			flashAddress = Utility.StringHex2Value((String)jsonObject.get(Defines.TAG_JSON_FW_CUST_FLASH_ADDRESS));
			flashNofRows                      =
					(long)jsonObject.get(Defines.TAG_JSON_FW_CUST_FLASH_NOF_ROWS);
			flashRowSize                      =
					(long)jsonObject.get(Defines.TAG_JSON_FW_CUST_FLASH_ROW_SIZE);
			flashSize = flashNofRows * flashRowSize;
			hwProcessorName                   =
					(String)jsonObject.get(Defines.TAG_JSON_HW_PROCESSOR_NAME);

			hwFlashPadding                      =
					(boolean)jsonObject.get(Defines.TAG_JSON_HW_FLASH_PADDING);
			hwFlashInt16Endian                =
					(String)jsonObject.get(Defines.TAG_JSON_HW_FLASH_INT16_ENDIAN);
			hwFlashInt32FromInt16             =
					(boolean)jsonObject.get(Defines.TAG_JSON_HW_FLASH_INT32_FROM_INT16);
			hwFlashInt32Endian                =
					(String)jsonObject.get(Defines.TAG_JSON_HW_FLASH_INT32_ENDIAN);
			hwFlashInt8UnevenAddressAllowed   =
					(boolean)jsonObject.get(Defines.TAG_JSON_HW_FLASH_INT8_UNEVEN_ADDRESS_ALLOWED);
			hwFlashInt16UnevenAddressAllowed  =
					(boolean)jsonObject.get(Defines.TAG_JSON_HW_FLASH_INT16_UNEVEN_ADDRESS_ALLOWED);
			hwFlashInt32UnevenAddressAllowed  =
					(boolean)jsonObject.get(Defines.TAG_JSON_HW_FLASH_INT32_UNEVEN_ADDRESS_ALLOWED);
			hwFlashStringUnevenAddressAllowed  =
					(boolean)jsonObject.get(Defines.TAG_JSON_HW_FLASH_STRING_UNEVEN_ADDRESS_ALLOWED);
			hwFlashStringStringUnevenAddressAllowed  =
					(boolean)jsonObject.get(Defines.TAG_JSON_HW_FLASH_STRINGSTRING_UNEVEN_ADDRESS_ALLOWED);
			
			
			if (hwFlashPadding) {
				targetDataSize = ((int)flashSize) * 2;
			} else {
				targetDataSize = (int)flashSize;
			}
			targetData = new byte[targetDataSize];
		} catch (FileNotFoundException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		} catch (ParseException e) {
			throw e;
		} finally {
			if (verbose) {
				System.out.println("------------- hw-fw-specifics: ------------- ");
				
				System.out.println("  hwTargetType                             = " +
								   hwTargetType);
				System.out.println("  flashAddress                             = " +
								   Utility.Value2StringHex(flashAddress, true));
				System.out.println("  flashNofRows                             = " +
								   flashNofRows);
				System.out.println("  flashRowSize                             = " +
								   flashRowSize);
				System.out.println("  flashSize                                = " +
								   flashSize);
				System.out.println("  hwProcessorName                          = " +
								   hwProcessorName);
				System.out.println("  hwFlashInt16Endian                       = " +
								   hwFlashInt16Endian);
				System.out.println("  hwFlashInt32FromInt16                    = " +
								   hwFlashInt32FromInt16);
				System.out.println("  hwFlashInt32Endian                       = " +
								   hwFlashInt32Endian);
				System.out.println("  hwFlashPadding                           = " +
								   hwFlashPadding);
				System.out.println("  hwFlashInt8UnevenAddressAllowed          = " +
						   		   hwFlashInt8UnevenAddressAllowed);
				System.out.println("  hwFlashInt16UnevenAddressAllowed         = " +
								   hwFlashInt16UnevenAddressAllowed);
				System.out.println("  hwFlashInt32UnevenAddressAllowed         = " +
						           hwFlashInt32UnevenAddressAllowed);
				System.out.println("  hwFlashStringUnevenAddressAllowed        = " +
						           hwFlashStringUnevenAddressAllowed);
				System.out.println("  hwFlashStringStringUnevenAddressAllowed  = " +
						           hwFlashStringStringUnevenAddressAllowed);
				
				System.out.println("--------------------------------------------");
			}
		}
	}

	/**
	 * Prints targetData as it looks for the moment.
	 * 
	 * @param columnWidth The width of the printed targetData (typically 16 or 32, but can be any number)
	 * @param leadingRowAddress If true, leading row information (relative addresses) are shown in the output.
	 * @param clearText If true, the hex formatted targetData is also shown as clear text in the end of each row.
	 */
	public void printTargetData(int columnWidth, boolean leadingRowAddress, boolean clearText) {
		Utility.printTargetData(targetData.length, targetData, columnWidth, leadingRowAddress, clearText);
	}

	/**
	 * Reads existing target data from the data that should be customized.
	 * Note: If this step is left out, the generated target data will be only zeroes.
	 * 
	 * @param inScanner Opened scanner that scans the Intel formatted hex text
	 * 
	 * @throws IOException On errors
	 * @throws CustomizerException On errors
	 */
	public void readTargetDataFromInputScanner(Scanner inScanner) throws IOException, CustomizerException {
		Scanner scan = null;
		
		if (inScanner == null) {
			if (filenameInput.length() > 0) {
				scan = new Scanner(new File(filenameInput));
			} else {
				throw new CustomizerException("Missing proper input data (No scanner, nor inputfile).");
			}
		} else {
			scan = inScanner;
		}
		long highAddress = 0;
		long paddingCompensate = 1;
		
		int[] nofRecordTypes = new int[Defines.INTEL_RECORD_TYPE_NOF];
		for (int i = 0; i < nofRecordTypes.length; i++) {
			nofRecordTypes[i] = 0;
		}
		
		if (hwFlashPadding) {
			paddingCompensate = 2;
		}
		
		scan.reset();
	    while(scan.hasNextLine()){
	        String line = scan.nextLine();

	        Record rcd = new Record(line);

			switch (rcd.recordType) {
				case Defines.INTEL_RECORD_TYPE_DATA:
					nofRecordTypes[rcd.recordType]++;
					
					long absoluteAddress = highAddress | rcd.address;
					
					if ((absoluteAddress >= flashAddress) &&
						(absoluteAddress < (flashAddress + (paddingCompensate * flashSize)))) {
						for (int i = 0; i < rcd.byteCount; i++) {
							targetData[(int)(absoluteAddress - flashAddress) + i] = (byte)rcd.data[i];
						}
					}
					
					break;
				case Defines.INTEL_RECORD_TYPE_END_OF_FILE:
					nofRecordTypes[rcd.recordType]++;
					break;
				case Defines.INTEL_RECORD_TYPE_EXTENDED_SEGMENT_ADDRESS:
					nofRecordTypes[rcd.recordType]++;
					break;
				case Defines.INTEL_RECORD_TYPE_START_SEGMENT_ADDRESS:
					nofRecordTypes[rcd.recordType]++;
					break;
				case Defines.INTEL_RECORD_TYPE_EXTENDED_LINEAR_ADDRESS:
					nofRecordTypes[rcd.recordType]++;
					highAddress = (rcd.data[0] << 24) | (rcd.data[1] << 16);
					break;
				case Defines.INTEL_RECORD_TYPE_START_LINEAR_ADDRESS:
					nofRecordTypes[rcd.recordType]++;
					break;
				default:
					throw new CustomizerException("Record type (" + rcd.recordType + ") not supported");
			}
	    }
	    
	    scan.close();
	    
		if (verbose) {
			System.out.println("nofRecordTypes = " + Arrays.toString(nofRecordTypes));
		}
	}

	/**
	 * Generates the Intel Hex formatted result.
	 * You can receive the result by calling getOutputIntelHex, see {@link #getOutputIntelHex()}
	 * 
	 * @param inScanner Opened scanner that scans the Intel formatted hex text
	 * 
	 * @throws IOException On errors
	 * @throws CustomizerException On errors
	 */
	public void generateIntelOutput(Scanner inScanner) throws IOException, CustomizerException {
		Scanner scan = null;
		
		if (inScanner == null) {
			if (filenameInput.length() > 0) {
				scan = new Scanner(new File(filenameInput));
			} else {
				throw new CustomizerException("Missing proper input data (No scanner, nor inputfile).");
			}
		} else {
			scan = inScanner;
		}
	
		long highAddress = 0;
		long paddingCompensate = 1;
		
		int[] nofRecordTypes = new int[Defines.INTEL_RECORD_TYPE_NOF];
		for (int i = 0; i < nofRecordTypes.length; i++) {
			nofRecordTypes[i] = 0;
		}
		
		if (hwFlashPadding) {
			paddingCompensate = 2;
		}
		
		outIntelHex = "";
		
		scan.reset();
	    while(scan.hasNextLine()){
	        String line = scan.nextLine();
	
	        Record rcd = new Record(line);
	
			switch (rcd.recordType) {
				case Defines.INTEL_RECORD_TYPE_DATA:
					nofRecordTypes[rcd.recordType]++;
					
					long absoluteAddress = highAddress | rcd.address;
					
					if ((absoluteAddress >= flashAddress) &&
						(absoluteAddress < (flashAddress + (paddingCompensate * flashSize)))) {
						for (int i = 0; i < rcd.byteCount; i++) {
							rcd.data[i] = (char)targetData[(int)(absoluteAddress - flashAddress) + i];
						}
						rcd.ReCalculateCheckSum();
					}
					
					break;
				case Defines.INTEL_RECORD_TYPE_END_OF_FILE:
					nofRecordTypes[rcd.recordType]++;
					break;
				case Defines.INTEL_RECORD_TYPE_EXTENDED_SEGMENT_ADDRESS:
					nofRecordTypes[rcd.recordType]++;
					break;
				case Defines.INTEL_RECORD_TYPE_START_SEGMENT_ADDRESS:
					nofRecordTypes[rcd.recordType]++;
					break;
				case Defines.INTEL_RECORD_TYPE_EXTENDED_LINEAR_ADDRESS:
					nofRecordTypes[rcd.recordType]++;
					highAddress = (rcd.data[0] << 24) | (rcd.data[1] << 16);
					break;
				case Defines.INTEL_RECORD_TYPE_START_LINEAR_ADDRESS:
					nofRecordTypes[rcd.recordType]++;
					break;
				default:
					throw new CustomizerException("Record type (" + rcd.recordType + ") not supported");
			}
			
			String sRcd = rcd.getIntelFormatString();
			//fileWriter.write(sRcd + "\n");
			outIntelHex += sRcd + "\n";
			
	    }
	    
	    scan.close();
	    
		if (verbose) {
			System.out.println("nofRecordTypes = " + Arrays.toString(nofRecordTypes));
		}
	}
	
	/**
	 * Receives the resulting Intel formatted Hex file.
	 * 
	 * @return Intel formatted Hex File as a String
	 */
	public String getOutputIntelHex() {
		return outIntelHex;
	}
	
	public void writeTargetDataToOutputFile() throws IOException, CustomizerException {
		Scanner scan = null;
		FileWriter fileWriter = null;
		
		if (filenameInput.length() > 0) {
			scan = new Scanner(new File(filenameInput));
		} else {
			throw new CustomizerException("No input file name set.");
		}
		
		if (filenameOutput.length() > 0) {
			fileWriter = new FileWriter(new File(filenameOutput));
		} else {
			scan.close();
			throw new CustomizerException("No output file name set.");
		}
	
		long highAddress = 0;
		long paddingCompensate = 1;
		
		int[] nofRecordTypes = new int[Defines.INTEL_RECORD_TYPE_NOF];
		for (int i = 0; i < nofRecordTypes.length; i++) {
			nofRecordTypes[i] = 0;
		}
		
		if (hwFlashPadding) {
			paddingCompensate = 2;
		}
		
	    while(scan.hasNextLine()){
	        String line = scan.nextLine();
	
	        Record rcd = new Record(line);
	
			switch (rcd.recordType) {
				case Defines.INTEL_RECORD_TYPE_DATA:
					nofRecordTypes[rcd.recordType]++;
					
					long absoluteAddress = highAddress | rcd.address;
					
					if ((absoluteAddress >= flashAddress) &&
						(absoluteAddress < (flashAddress + (paddingCompensate * flashSize)))) {
						for (int i = 0; i < rcd.byteCount; i++) {
							rcd.data[i] = (char)targetData[(int)(absoluteAddress - flashAddress) + i];
						}
						rcd.ReCalculateCheckSum();
					}
					
					break;
				case Defines.INTEL_RECORD_TYPE_END_OF_FILE:
					nofRecordTypes[rcd.recordType]++;
					break;
				case Defines.INTEL_RECORD_TYPE_EXTENDED_SEGMENT_ADDRESS:
					nofRecordTypes[rcd.recordType]++;
					break;
				case Defines.INTEL_RECORD_TYPE_START_SEGMENT_ADDRESS:
					nofRecordTypes[rcd.recordType]++;
					break;
				case Defines.INTEL_RECORD_TYPE_EXTENDED_LINEAR_ADDRESS:
					nofRecordTypes[rcd.recordType]++;
					highAddress = (rcd.data[0] << 24) | (rcd.data[1] << 16);
					break;
				case Defines.INTEL_RECORD_TYPE_START_LINEAR_ADDRESS:
					nofRecordTypes[rcd.recordType]++;
					break;
				default:
					throw new CustomizerException("Record type (" + rcd.recordType + ") not supported");
			}
			
			String sRcd = rcd.getIntelFormatString();
			fileWriter.write(sRcd + "\n");
	    }
	    
	    scan.close();
	    
		if (verbose) {
			System.out.println("nofRecordTypes = " + Arrays.toString(nofRecordTypes));
		}
		
		fileWriter.flush();
		fileWriter.close();
	}

	/**
	 * Receives the target data as it looks for the moment
	 * 
	 * @return targetData as an array of bytes
	 */
	public byte[] getTargetData() {
		return targetData;
	}
	
}
