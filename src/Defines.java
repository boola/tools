
public class Defines {

	/*
	 * Command line arguments
	 */
	final static String ARG_INPUT =
			"in=";
	final static String ARG_CUSTOMIZATION =
			"cust=";
	final static String ARG_OUTPUT =
			"out=";
	final static String ARG_VERBOSE =
			"verbose=";
		final static String ARG_VERBOSE_CUSTOMIZE_HELPER =
				"customizehelper";
		final static String ARG_VERBOSE_MAIN =
				"main";
	
	final static String ARG_FLASH_ADDRESS =
			"flash-address=0x";
	final static String ARG_TARGET_TYPE =
			"target-type=";
		final static String TARGET_TYPE_IDESK =
				"idesk";
		final static String TARGET_TYPE_SENSE =
				"sense";
		final static String TARGET_TYPE_ID_MINI =
				"idmini";
	
	/*
	 * JSON customization tags
	 */
	final static String TAG_JSON_CUSTOMIZATION_FW_HW_SPECIFICS =
			"fw-hw-specifics";
	final static String TAG_JSON_HW_TARGET_TYPE =
			"hw-target-type";
	final static String TAG_JSON_FW_CUST_FLASH_ADDRESS =
			"fw-customization-flash-address";
	final static String TAG_JSON_FW_CUST_INTEL_ADDRESS =
			"fw-customization-intel-address";
	final static String TAG_JSON_FW_CUST_FLASH_NOF_ROWS =
			"fw-customization-flash-nof-rows";
	final static String TAG_JSON_FW_CUST_FLASH_ROW_SIZE =
			"fw-customization-flash-row-useful-data-size";
	final static String TAG_JSON_HW_PROCESSOR_NAME =
			"hw-processor-name";
	final static String TAG_JSON_HW_FLASH_INT16_ENDIAN =
			"hw-flash-int16-endian";
	final static String TAG_JSON_HW_FLASH_INT32_FROM_INT16 =
			"hw-flash-int32-constructed-by-int16";
	final static String TAG_JSON_HW_FLASH_INT32_ENDIAN =
			"hw-flash-int32-endian";
	final static String TAG_JSON_HW_FLASH_PADDING =
			"hw-flash-padding";
	public static final Object TAG_JSON_HW_FLASH_INT8_UNEVEN_ADDRESS_ALLOWED =
			"hw-flash-restrictions-uneven-int8-address-allowed";
	public static final Object TAG_JSON_HW_FLASH_INT16_UNEVEN_ADDRESS_ALLOWED =
			"hw-flash-restrictions-uneven-int16-address-allowed";
	public static final Object TAG_JSON_HW_FLASH_INT32_UNEVEN_ADDRESS_ALLOWED =
			"hw-flash-restrictions-uneven-int32-address-allowed";
	public static final Object TAG_JSON_HW_FLASH_STRING_UNEVEN_ADDRESS_ALLOWED =
			"hw-flash-restrictions-uneven-string-address-allowed";
	public static final Object TAG_JSON_HW_FLASH_STRINGSTRING_UNEVEN_ADDRESS_ALLOWED =
			"hw-flash-restrictions-uneven-string-string-address-allowed";

	final static String TAG_JSON_DEVICE_CONFIG =
			"device-config";
	final static String TAG_JSON_SENSOR_CONFIG =
			"sensor-config";
	final static String TAG_JSON_WLAN_1_CONFIG =
			"wlan-config-1";
	final static String TAG_JSON_WLAN_2_CONFIG =
			"wlan-config-2";
	final static String TAG_JSON_KEY_CONFIG =
			"key-config";
	
	public static final int INTEL_RECORD_TYPE_DATA = 0;
	public static final int INTEL_RECORD_TYPE_END_OF_FILE = 1;
	public static final int INTEL_RECORD_TYPE_EXTENDED_SEGMENT_ADDRESS = 2;
	public static final int INTEL_RECORD_TYPE_START_SEGMENT_ADDRESS = 3;
	public static final int INTEL_RECORD_TYPE_EXTENDED_LINEAR_ADDRESS = 4;
	public static final int INTEL_RECORD_TYPE_START_LINEAR_ADDRESS = 5;
	
	public static final int INTEL_RECORD_TYPE_NOF = 6;
	
	public static final Object TAG_JSON_FLASH_ROW_START_INDEX =
			"flash-row-start-index";
	public static final Object TAG_JSON_FLASH_START_INDEX =
			"flash-start-index";
	public static final Object TAG_JSON_FLASH_SIZE =
			"flash-size";
	public static final Object TAG_JSON_DATA_TYPE =
			"data-type";
	public static final Object TAG_JSON_DATA =
			"data";
	public static final Object TAG_JSON_VALUE_NAME =
			"value-name";
	public static final Object TAG_JSON_BIT_FIELDS =
			"bit-fields";
	public static final Object TAG_JSON_BIT_FIELD_START_INDEX =
			"bit-field-start-index";
	public static final Object TAG_JSON_BIT_FIELD_NOF_BITS =
			"bit-field-nof-bits";
	public static final Object TAG_JSON_DATA_TYPE_BYTE_ARRAY =
			"byte-array";
	public static final Object TAG_JSON_DATA_TYPE_UINT =
			"uint";
	public static final Object TAG_JSON_DATA_TYPE_STRING =
			"string";
	public static final Object TAG_JSON_DATA_TYPE_FILLER =
			"filler";
	public static final Object TAG_JSON_DATA_TYPE_BIT_FIELD =
			"bit-field";
	public static final Object TAG_JSON_DATA_TYPE_IP_ADDRESS =
			"ip-address";
	public static final Object TAG_JSON_FLASH_SPECIAL =
			"flash-special";
	public static final Object TAG_JSON_ORDER =
			"order";
}
